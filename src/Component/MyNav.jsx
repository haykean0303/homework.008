import React from 'react'
import {Navbar} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function MyNav() {
    return (
        <div>
            <Navbar>
            <Navbar.Brand href="#home">KSHRD Student</Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                Signed in as: <a href="#login">Huort Seanghay</a>
                </Navbar.Text>
            </Navbar.Collapse>
            </Navbar>
        </div>
    )
}
