import React from 'react'
import {Table, Button} from "react-bootstrap";

function MyTable(props) {

    const temp = props.items.filter(item =>{
        return item.id > 0
    })

    return (
        <div>
            <h2>Table Account</h2>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                        {
                            temp.map((item,i) =>(
                            <tr 
                                className={item.isSelected === true ? "bg-blue text-white":""}
                                key={i} 
                                onClick={()=> {
                                    props.onGetSelect(i)
                                }}>
                                {
                                    <td>{item.id}</td>
                                }
                                {
                                    <td>{item.username}</td>
                                }
                                {
                                    <td>{item.email}</td>
                                }
                                {
                                    <td>{item.gender}</td>
                                }
                            </tr>
                            )) 
                        } 
                </tbody>
            </Table>
            <Button variant="danger" type="button"
            > Delete
            </Button>
        </div>
    )
}
export default MyTable;
