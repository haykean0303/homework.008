import './App.css';
import MyNav from './Component/MyNav';
import MyContents from './Component/MyContents';
import React, { Component } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import MyTable from './Component/MyTable';


class App extends Component {

  constructor() {
    super();
    this.state = {
        data : [{
                id: 0,
                username: "",
                gender: "",
                email: "",
                password: "", 
                isSelected: true,
            }],
            newId : 1
        }

        this.onGetData = this.onGetData.bind(this)
        this.onGetSelect = this.onGetSelect.bind(this)
   }

  onGetData(data){
    let addData = { 
                    id : this.state.data.length,
                    username : data.username,
                    gender : data.gender,
                    email : data.email, 
                    password : data.password, 
                    isSelected : false
                  }
    let newData = [...this.state.data, addData]
    this.setState({
      data : newData
    })
  }

  onGetSelect(id){
    let temp = this.state.data.filter(item =>{
      return item.id > 0
    })
    let data = [...temp]
    data[id].isSelected = !data[id].isSelected
    this.setState({
      data : data
    })
  }

  render() {
    return (
      <Container>
        <MyNav/>
        <Row className="m-4">
          <Col sm="4">
            <MyContents
              onGetData = {this.onGetData}
            />
          </Col>
          <Col sm="8">
            <MyTable 
              items={this.state.data}
              onGetSelect = {this.onGetSelect}     
              />
          </Col>
        </Row>
      </Container>
    )
  }
}
export default App;

